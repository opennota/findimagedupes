// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"errors"
	"strconv"
)

type thresholdVar int

var ThresholdAuto = thresholdVar(-1)

// errParse is returned by Set if a flag's value fails to parse, such as with an invalid integer for Int.
var errParse = errors.New("parse error")

// errRange is returned by Set if a flag's value is out of range.
var errRange = errors.New("value out of range")

func numError(err error) error {
	ne, ok := err.(*strconv.NumError)
	if !ok {
		return err
	}
	if ne.Err == strconv.ErrSyntax {
		return errParse
	}
	if ne.Err == strconv.ErrRange {
		return errRange
	}
	return err
}

func (v thresholdVar) String() string {
	if v == -1 {
		return "auto"
	}
	return strconv.Itoa(int(v))
}

func (v thresholdVar) IsBoolFlag() bool {
	return false
}

func (v *thresholdVar) Set(val string) error {
	if val == "auto" {
		*v = ThresholdAuto
		return nil
	}
	i, err := strconv.ParseInt(val, 0, strconv.IntSize)
	if err != nil {
		err = numError(err)
	} else if i < 0 || i > 63 {
		err = errRange
	}
	*v = thresholdVar(i)
	return err
}
